import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/events.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flame/components.dart';

import 'paper_component.dart';
import 'rock_component.dart';
import 'scissors_component.dart';

void main() {
  runApp(
    GameWidget(
      game: MyGame(),
    ),
  );
}

class MyGame extends FlameGame
    with TapCallbacks, CollisionCallbacks, HasCollisionDetection {
  @override
  Future<void> onLoad() async {
    add(ScreenHitbox());
  }

  @override
  void onTapDown(TapDownEvent event) async {
    super.onTapDown(event);
    if (!event.handled) {
      List<Component> items = [];
      for (int i = 0; i < 4; i++) {
        items.add(Rock(Vector2(
          Random().nextInt(500).toDouble(),
          Random().nextInt(1000).toDouble(),
        )));
        items.add(Scissors(Vector2(
          Random().nextInt(500).toDouble(),
          Random().nextInt(1000).toDouble(),
        )));
        items.add(Paper(Vector2(
          Random().nextInt(500).toDouble(),
          Random().nextInt(1000).toDouble(),
        )));
      }

      addAll(items);
    }
  }
}
