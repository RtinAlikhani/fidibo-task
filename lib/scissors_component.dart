import 'package:flame/collisions.dart';
import 'package:flutter/material.dart';
import 'package:flame/components.dart';

import 'main.dart';
import 'rock_component.dart';

const _style = TextStyle(color: Colors.red, fontSize: 30);
final regular = TextPaint(style: _style);

class Scissors extends CircleComponent
    with HasGameRef<MyGame>, CollisionCallbacks {
  late Vector2 velocity;
  final _speed = 120.0;
  static const ballRadius = 15.0;
  var _isCollision = false;

  int xDirection = 1;
  int yDirection = 1;

  Scissors(this.initialPosition)
      : super(
            radius: ballRadius,
            position: initialPosition,
            anchor: Anchor.center,
            children: [
              TextComponent(text: " S", textRenderer: regular),
            ]) {
    add(CircleHitbox());
  }

  final Vector2 initialPosition;

  @override
  Future<void> onLoad() async {
    await super.onLoad();
    final center = gameRef.size / 2;
    velocity = (center - position)..scaleTo(_speed);
  }

  @override
  void update(double dt) {
    super.update(dt);

    x += xDirection * _speed * dt;
    y += yDirection * _speed * dt;

    final rect = toRect();

    if ((rect.left <= 0 && xDirection == -1) ||
        (rect.right >= gameRef.size.x && xDirection == 1)) {
      xDirection = xDirection * -1;
    }

    if ((rect.top <= 0 && yDirection == -1) ||
        (rect.bottom >= gameRef.size.y && yDirection == 1)) {
      yDirection = yDirection * -1;
    }

    if (_isCollision) {
      _isCollision = false;
      removeFromParent();
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is! ScreenHitbox) {
      xDirection = xDirection * -1;
      yDirection = yDirection * -1;
    }
    if (other is Rock) {
      _isCollision = true;
      return;
    }
  }
}
